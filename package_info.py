import csv
import pkg_resources

def get_pkg_license(pkg):
    try:
        lines = pkg.get_metadata_lines('METADATA')
    except:
        lines = pkg.get_metadata_lines('PKG-INFO')

    for line in lines:
        if line.startswith('License:'):
            return line[9:]
    return '(License not found)'

def get_package_info(pkg):
    package_info = {}
    metadata = pkg.get_metadata_lines('METADATA')
    license = '(License not found)'
    for meta_d in metadata:
        if meta_d.startswith('License:'):
            license = meta_d[9:]
    package_details = str(pkg).split()
    package_info['package_name'] = package_details[0]
    package_info['package_version'] = package_details[1]
    package_info['package_license'] = license
    return package_info

def generate_packages_and_licenses_csv():
    package_list = []
    columns = ['package_name','package_version','package_license']
    packages_set = sorted(pkg_resources.working_set, key=lambda x: str(x).lower())
    for pkg in packages_set:
        package_list.append(get_package_info(pkg))
    
    with open('packages-info.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames = columns)
        writer.writeheader()
        writer.writerows(package_list)


if __name__ == "__main__":
    generate_packages_and_licenses_csv()
