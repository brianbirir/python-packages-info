# Python Packages Info

Derives python packages information using the `pkg_resources` library and then saved in a CSV file. The script needs to be run in a virtual environment to get the Python packages installed in that environment and not in the system environment.
